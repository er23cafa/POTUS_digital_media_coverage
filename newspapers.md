#### USA:
[newspaper run size US](https://www.statista.com/statistics/184682/us-daily-newspapers-by-circulation/)
* USA Today
* Wall Street Journal
* NYT

New York Times: 
* [instruction](https://developer.nytimes.com)
* [guide](https://developer.nytimes.com/branding)
* [How to use the NYT Article Search API](https://data-gov.tw.rpi.edu/wiki/How_to_use_New_York_Times_Article_Search_API)
* -> JSON format
* -> returns first 10 results, no way to change the number of results returned - use offset to get the next results 

The Guardian: 
* [instruction](http://open-platform.theguardian.com/access/)
* -> JSON/XML format

Huffington Post Pollster:
!!!!!! only tracks public opinion !!!!!!
* [introduction](https://app.swaggerhub.com/api/huffpostdata/pollster-api/2.0.0)
* -> JSON/XML

Guardian api
* [test call](https://content.guardianapis.com/search?format=json&api-key=5fa3c245-288c-4ff8-a5d5-b3423e6aa87e&q=president%20OR%2Otrump&from-date=2017-01-20&to-date=2017-02-20&use-date=first-publication&show-fields=body,headline,trailText,thumbnail,shortUrl,wordcount&page-size=200)
* http://open-platform.theguardian.com/documentation/search
* http://content.guardianapis.com/search?from-date=2017-01-20&to-date=2017-02-20&section=us-news&order-by=oldest&page-size=200&show-blocks=body&page=1&q=(donald%20AND%20trump)%20OR%20(president%20AND%20trump)&api-key=5fa3c245-288c-4ff8-a5d5-b3423e6aa87e
* http://content.guardianapis.com/search?from-date=2009-01-20&to-date=2009-02-20&section=us-news&order-by=oldest&page-size=200&show-blocks=body&page=1&q=(barack%20AND%20obama)%20OR%20(president%20AND%20obama)&api-key=5fa3c245-288c-4ff8-a5d5-b3423e6aa87e