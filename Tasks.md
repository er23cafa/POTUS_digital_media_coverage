Tasklist
========

#### Please mark the completed tasks with a #myName and a * [x]
#### Do not delete completed tasks, as we might need them to document our progress

* [x] Read the guide Köntges gave us for writing better project reports
* [ ] Decide on Java vs Python vs R
* [ ] Search for the accessability of our sources via automatic programming means
* [ ] Add the List of outlets we want to use
* [ ] Contact the selected news outlets, to check if we can be granted access to their digital-databases
* [x] Decide on "Using a toolkit for topic modelling vs. implementing the algorithm ourself" #Gero #Emil (Decided on using a toolkit)

* [x] Produce something that we can show to Koentges until monday evening
* [ ] Start the database creation
* [ ] Install Latex
* [x] Get Rstudio running 