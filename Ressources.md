This is the Ressource List, which mainly contains Links on methods we use or might use
======================================================================================

##### Topic modelling:
----------------
* [A coprehensive assortment of information on topic modelling](http://www.scottbot.net/HIAL/index.html@p=19113.html)

#####    These scientific papers are important for topic modelling:
  * [Latent Dirichlet Allocation (LDA) paper](http://www.jmlr.org/papers/volume3/blei03a/blei03a.pdf)
  * [Graphs, Maps, Trees by Franco Moretti (originally a book)](http://www.mat.ucsb.edu/~g.legrady/academic/courses/09w259/Moretti_graphs.pdf)
  * [Rethinking LDA: Why Priors Matter](https://mimno.infosci.cornell.edu/papers/NIPS2009_0929.pdf)
  * [LDAvis: A method for visualizing and interpreting topics (for understanding our visualization)](http://nlp.stanford.edu/events/illvi2014/papers/sievert-illvi2014.pdf)

##### Markdown:
---------
* [markdown cheatsheet](http://assemble.io/docs/Cheatsheet-Markdown.html)

##### Citations for background and arguments:
---------------------------------------
* [Argument for topic modelling the whole corpus over the individual documents?](http://dsl.richmond.edu/dispatch/pages/intro)

##### Writing
* [Guide to Writing a Project Report](https://newton.ex.ac.uk/handbook/PHY/forms/WLB010919-4.pdf)
* [How to write a project report](www.cs.york.ac.uk/projects/howtowrt.html)
 

##### Latex
* [A not so short indroduction to Latex](http://mirror.hmc.edu/ctan/info/lshort/english/lshort.pdf)


##### Gero Api Key Digital Nz
* "XhhyQVK8Gu6i9kjMvcWx"