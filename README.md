Readme :rocket:
===============
Our final projectreport can be found [here](https://git.informatik.uni-leipzig.de/er23cafa/POTUS_digital_media_coverage/blob/master/_Projectreport_NoraBrummel_EmilRode_GeroHarzer.pdf)

### The goal of the project is, to analyse the representation of the last POTUSes (Presidents of the United States) in mayor media outlets,
### focussing on the online articles of print media

* The [Tasks.md](https://git.informatik.uni-leipzig.de/er23cafa/POTUS_digital_media_coverage/blob/master/Tasks.md) contains the tasks which need to be handled.
* The [Ressources.md](https://git.informatik.uni-leipzig.de/er23cafa/POTUS_digital_media_coverage/blob/master/Ressources.md) which should be structured by the category of links and a short description of every link.
* The [report_structure.md](https://git.informatik.uni-leipzig.de/er23cafa/POTUS_digital_media_coverage/blob/master/Report_structure.md) is a pitch of the report which outlines basic structures
* The [Background_Structure.md](https://git.informatik.uni-leipzig.de/er23cafa/POTUS_digital_media_coverage/blob/master/Background_Structure.md) contains the substructure of the background section and its subjects 
* 