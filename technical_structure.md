### In the analytical section, we are trying to outline the process we went through to analyse our data.

* Api calls and news networks
* datacleaning with openrefine
* datacleaning in r
* topic moddelling theory (-> background?)
    * LDAvis  
* topic moddelling in r
    * script discussion 
* visualization## Analytical section
* 


## Analytical section
### Corpus creation
#### Corpus baseline
To create a meaningfull corpus for topic modeling we have to establish parameters that help us to
systematically gather our data. These parameter are of crucial importance, because we need uniform
data to apply our algorithm to. Parameters get especially important when pulling data from various 
newsnetworks, as we originally planned.  More often than not the format and fields where data is
provided vary greatly between outlets. We settled for the following parameters:

* data from the biggest anglophone newsoutlet that enables full article pulling via api, "The Guardian" 
* first timeframe from 01/20/2009 - 02/20/2009 (Barack Obama's first month in office)
* second timeframe from 01/20/2017 -02/20/2017 (Donald Trump's first month in Office)
* The article must contain either: "name, surname" or "president, surname" 
* dataformat .json


The data in need for the topic modelling process is basically the raw article text, to keep our data
organised we pulled the it into open refine and kept the webUrl, webTitle, wordcount and raw html body
as additionall categories.

#### Api Calls
The british newsoutlet "The Guardian" features great api-service called open-platform that offers 
full article text and an extensive spectrum of categories to display at wish. We have build up our call
acording to the parameters established previously:

1. http://content.guardianapis.com/search?
	The content domain of the open platform api
1. from-date=20XX-01-20&to-date=20XX-02-20
	Our timeframe depending the different office holder
1. &section=us-news
	We only use the US-news section to ensure our corpus would not be polluted by non related articles
1. &page-size=200
	We set the page size to the maximum limit, to enable as few calls as possible
1. &show-fields=body,wordcount,headline
	The "show-fields" command allows us to add more specific content to our call. Most important
	here is the "body" field, as it requests the html-body of the article
	Note: The command "show-blocks=body" returns a block that is called "body_text_summary" which is a
	clean article text block, however since it also returns an extensive amount of other blocks
	(eg. each picture is listed as a block) we crashed OpenRefine as we tried to process the files.	
1.&page=1
	The standard page number field
1. &q=(name%20AND%20surname)%20OR%20(president%20AND%20surname)
	This is our search therm, we used both the "AND" and "OR" function which open-platform offers to
	connect our results and avoid to call the same article twice
1. &api-key=my_api_key
	pretty self explanatory
All the piece set together we managed to create a quite convincing call, that returned us about 800 
results. We downloaded them all into an OpenRefine project to sift through our data.

#### Data cleaning challenges
As we didnt manage to call the clean article text without crashing OpenRefine, we decided to clean up
the html-body in OpenRefine. While the articles from 2009, are mostly html free, the articles from 
2017 are full of in-buildt links, social media references and in-text images, that spoil the data. 
We found a recouring pattern of html tags ("<" & ">") and came up with a regualar expression to capture
all the content between these tags, and the tags themselves 
	
	>>  /<[^>]*>/ 
	The "/" marks the start and end of the regular expression
	The "<" marks the first character 
	The "*" tells the regex to capture any character until ">"
	The [^>] is an exception that stops the regex at ">" so it only catches the content between
	 "<" and ">"

We implemented this regular expression with great success in OpenRefine via GREL (General Refine Expression Language)
in following function

	>> value.replace(/<[^>]*>/, "").replace(/\[[^\]]*\]/, "")
	"replace" the easiest way to delete something in GREL as you can just replace it with nothing ("")
	
	>> /\[[^\]]*\]/
	this the same regular expression patter for capturing everything in square brackets, the only
	problem proves to be that it is a function itself so you need to escape it with a backslash "\"

With this serial data cleaning implemented we managed to create a raw text corpus, which we then exported
into json, for applying our scricpt. Only the the export process itself created pollution (empty lines,
unix characters and more) but before we talk about the data cleaning in R, we want to establish an 
unterstanding for the the methodology of topic modeling.


#### Using regex in Open refine to clean our corpus
### Topic modeling 
#### Topic modeling intro (=abstract)

Topic modeling is a method to define a pattern of co-occuring words in a set vocabulary that is  called a topic. A topic in the sense of topic modeling is not a congruence to its linguistic counterpart but rather a statistical distribution of related tokens in a dataset. 
There are various topic models, but the general method is distrubuting all tokens of a corpus over a set number of topics. This number (k) depends on the size of the corpus and the distribution of specific words over the corpus. 
E.g. given all of Wikipedia as a corpus it would be more feasible to use a much higher of topics, than a corpus of a regional newspaper consisting out of 500 articles. 
Digital Humanits generally agree that K is not absolut but rather relative. 
#Topic models assume a general exchangeability of a word in a given context, meaning a word is definte byits neighbours. 

To understand topic modeling on a conceptual level we need to talk about tf-idf. Tf-idf (term frequency -inverse document frequency) is a matrix that ranks words in a corpus based on how much information they carry. To accompish that, it divides the overall term frequency with the "regional density" of a term in a corpus, meaning a term that has medium density all over the corpus carries less information then a term that has a really high density in one document but does not occur otherwise in a corpus. Tf-idf
ranks how special terms are.

The next step on the ladder was made through latent semantic analysis (LSA) by trying to find out how related terms are. LSA does this by comparing the tf-idf score of a term with the tf-idf scores of the terms in the context. This process is called singular value decomposition, which assumes that terms are more related when they appear in the same context. That means terms with a high tf-idf score, that are in context with each other are highly related.

Nevertheless LSA does not talk about topics, this idea was first introduced with probabilistic latent semantic analysis (pLSA), which can be summarized as the idea of an additional layer between words and documents (1). 

The breaktrough was made 2003 with the Latent Dirichlet Distribution (LDA) by David Blei, which we will
now discuss a little more in detail.



### visualization
In our annotated R script we set the number of topics at 15 and 20,  we set our alpha and our beta
to 0.02 and 0.2 respectivly, and let the 'gibbs sampler'-function, provided by the 'lda' package in R,
run it with 1000 sweeps, thus creating two different topic models to interpret. These topic models are 
then visualized by the 'LDAvis'-package. The number of therms displayed in the visualization is set to 25.
In the resulting html-document, an additional variable 'lambda' is provided. 

####Lambda 
Lambda is weight parameter, and "determines the weight given to the probability  of term w under the topic
k" (2). That means that a lambda closer to 1 will display the most used tokens in a topic while a lambda 
closer to 0 means that the LDAvis will display the more tokens that are only used in said topic. Sievert 
conducted a small user study to determine  "an 'optimal' value of lambda for topic interpretation (2)", 
suggested a value of 0.6, as in said study the user recognitions of the predetermined topics in the corpus
were best with a visualization that had a lambda around this value.

####LDAvis
LDAvis provides the visualization through an html document. This document is divided in an graphical displayal
of the topic on an intertopic distance map on the left, and a a topic specific wordlist organised in a bar
chart at the right. 
In the intertopic distance map "the area of the circles are proportional to the relative 
prevalence of the topics in the corpus" (2) and the 'intertopic distance' (how topics are connected to each other)
is displayed by the positions of the circles on the plane.#
The bar chart ranks a number of topic specified terms set with the variable 'R' which describes the numbe
of terms displayed and ranks them accourding to their topic specific relevance. Visualized through a grey bar,
representing the "corpus wide frequency of each term", and a red bar representing the topic specific frequency.
When altering lambda as described above, the bar chart will rank term by the width of the red bar from top
to bottom when lambda is close 1 and setting lambda to 0 will rank therms solely on how specific they are for 
the given topic (grey and red bar of nearly equal width)
LDAvis offers the option to hoover over a term to display its conditional distribution over topic on the 
intertopic distance map.


### Our data
Our analysis and discussion is based on the visualization with 20 topics, 25 topic specific terms each and a 
lambda of 0.6, the 25 most relevant terms are organised in a table which can be found in the appendix
By hoovering over the terms as explained previously, we tried to determine the topics most assosiated with 
'donald' and 'trump' and with 'barack' and 'obama' respectively. Please note that this categorization does not 
imply that only these exclusive topics are connected to the administration only to the specific tokens used, 
as to be seen in 'topic 11' for example which contains words so as "sessions, jeff, devos, confirmation, nominees,
cabinet" although Betsy Devos and Jeff Sessions belong to Donald Trumps cabinet, this topic does#
The topics most associated with 'trump' are in descending order 1, 12, 5, 3 and 7, the topics most associate with
'donald' are 1, 12, 5, 3, 18 and 7. 
The topics most associated with 'barack' are 6, 1, 4, 13, 9, and 16, for the token 'obama' the same topic are 
most prevalent but the topic 2 has significantly less weight.
Overall the topic distribution shows two large topics 1 and 2 that are isolated from the rest of the topics but
show a large amount of overlap (the intertopic distance in LDAvis is calculated by Jensen-Shannon Divergence),
these topics make up 28,7 and 14,7 percent of the tokens. 
There is a large cluster of smaller topics that overlap in two partial clusters. Most of this cluster is neither
associate with 'trump' nor 'obama' While Obama shows more association


### Interpretation 

The big cluster discussed above could be a collection of smaller political topics that are usually discussed in
a politics articels in a newspaper. It spans over the themes of "affordable care act", racism (especially topic 20),
foreign relations, voter fraud, investigation into personal relations with russia, guantanamo,  
 



###### LDA
## Script
###### Data Cleaning in R
###### formating our data to fit the script
###### stopwords
###### white- & blacklist implementation
###### Topic modeling algorithm
###### itterations & k

