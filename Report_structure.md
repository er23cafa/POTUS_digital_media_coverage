# Report structure

##  Pages
paragraph | number of pages | Content
------------ | ------------ | -----------
Introduction | 1-2 | Influence of Digital media in general 
Background | 3-4 | Role of media in the US, [Atlantikbrücke,](https://de.wikipedia.org/wiki/Atlantik-Brücke) relevance of the project, social background, bias discussion & problems
Analytical | 5 | corpus creation, metadata, topic modelling, visualisation process,
Discussion | 3-4 | key issues, significance of results, specific bias of the research
Conclusion | 1-2 | future outlook, what else could we have done
------------ | ------------ | -----------
Sum | 13-17 | give a comprehensive comparison and possible explanations of key issue during the first month in office of a new PotUS namely Obama & Trump

## What we are trying to work out
The first officials acts of an US-president have always been crucial to evalue 
how the four years of the therm will impact the politics. Thus the media plays a 
huge role in evaluatin g the work of a freshly inaugurated US-President. We are 
trying work out the issues represented in the media, during the first month of 
this therm. Are they more of political or personal nature? Can you draw 
conclusions whether the president is sucessesful or not? So we come to following
research question:

***What are the key issues represented in they media during the first month of a new PotUS, and are they of personal or political nature?***


## So what?
The relationship of Trump and the media has always been an uneasy one. Does this mean
media is starting to report about Trump in mostly personal matters or are they still 
focused on the politcs. 

To compare this, Obama the previous office holder is an obvious comparison to draw when discussing this relationship

*How does the ratio between personal and political representation vary from different candidates?*

In a broader sense this poses the question whether we can determine a ratio at all.



## How to

We are creating a dataset out of printmedia articles from the first month of a presidents therm.
Therefore we have established differents parameters to systhematically gather our data.

* Corpus parameters
    * Data from the three biggest Newsoutlets that publish printmedia in the USA
        * Usa Today
        * Wall Street Journal
        * New York Times
    * Data from the 20.01.2017-20.02.2017 and 20.01.2008- 20.02.2008
    * Name of the office holder in the title
    * Identifier
        * language _ newsoutlet _  articleindentifier _ type
        * type: comment or article
        * articleindentifier: starting from 0000 

With this corpus at hand we will try to model the recouring topics.

abstract tasks | specific tasks | annotations
-------------- | -------------- | -----------
corpus | Newsmedia article | see: "corpus parameters"
technique | software used | vanilla LDA, MALLET
unit of analysis | articles ||
post processing | interpretation of our data | |
visualization | tbd | |















