Consult the [ressources.md](https://git.informatik.uni-leipzig.de/er23cafa/POTUS_digital_media_coverage/blob/master/Ressources.md) for more sources


>Latent Dirichlet allocation (LDA) is a generative probabilistic model of a corpus. The basic idea is
that documents are represented as random mixtures over latent topics, where each topic is characterized
by a distribution over words.
[_source_](http://www.jmlr.org/papers/volume3/blei03a/blei03a.pdf)